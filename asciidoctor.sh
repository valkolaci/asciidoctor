#!/bin/bash

dir=$(realpath $(dirname "$0"))
uid=$(id -u)
gid=$(id -g)

ASCIIDOCTOR_VER=1.17.0
ASCIIDOCTOR_IMAGE="asciidoctor/docker-asciidoctor:$ASCIIDOCTOR_VER"

docker run --rm -ti -e TMP=/tmp -e TEMP=/tmp -e HOME=/tmp -v "${dir}:/documents/" -u "${uid}:${gid}" --name asciidoctor "${ASCIIDOCTOR_IMAGE}" "$@"
