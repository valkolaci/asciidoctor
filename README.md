# asciidoctor

Scripts to use asciidoctor in a Docker container

## asciidoctor.sh

Run asciidoctor in a Docker container

Current Docker image version: `asciidoctor/docker-asciidoctor:1.17.0`

Example:

```
asciidoctor.sh asciidoctor-pdf -o output.pdf input.adoc
```

## mkdocimage.sh

Run asciidoctor with parameters `-r asciidoctor-diagram` to generate
diagrams (uses asciidoctor.sh)

Example:

```
mkdocimage.sh doc.adoc
```
