#!/bin/bash

dir=$(realpath $(dirname "$0"))

ASCIIDOCTOR=("$dir/asciidoctor.sh" asciidoctor -r asciidoctor-diagram)

"${ASCIIDOCTOR[@]}" "$@"
